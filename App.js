const { Container, Grid, Box } = require("@mui/material"); 
 
function App() { 
  return ( 
    <Container> 
      <Grid container> 
        <Grid item xs={2}  md={2} > 
          <Box sx={{height:150, backgroundColor:"red"}}></Box> 
        </Grid> 
        <Grid item xs={2} md={2}> 
          <Box sx={{height:150, backgroundColor:"blue"}}></Box> 
        </Grid> 
        <Grid item xs={2} md={2}> 
          <Box sx={{height:150, backgroundColor:"yellow"}}></Box> 
        </Grid> 
        <Grid item xs={2} md={2}> 
          <Box sx={{height:150, backgroundColor:"green"}}></Box> 
        </Grid> 
        <Grid item xs={2} md={2}> 
          <Box sx={{height:150, backgroundColor:"pink"}}></Box> 
        </Grid> 
        <Grid item xs={2} md={2}> 
           
        </Grid> 
      </Grid> 
    </Container> 
  ); 
} 
 
export default App;
